<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>How to Choose the Right UX Metrics for Your Product</title>
        <meta name="description" content="Choosing the right UX metrics for your product can be a tough challenge. The HEART framework by Google can help you proceed with confidence.">
        <meta name="keywords" content="UX Metrics, User Experience Design, HEART Framework, Goals Signals Metrics, User Experience">
        <meta name="author" content="Digital Telepathy, Google Ventures">
        <meta name="viewport" content="width=1150, user-scalable=0, minimal-ui">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,300' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/styles.css">

        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script>

<script type="text/javascript">(function() {
  var fl = document.createElement('script'); fl.type = 'text/javascript'; fl.async = true; fl.src = document.location.protocol + '//filamentapp.s3.amazonaws.com/63c029185b024d8d779deb2b808fcc9d.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(fl, s);
})();</script>
  